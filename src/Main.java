import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import java.util.Random;


public class Main extends BasicGame
{
	
	
	public static int WIDTH = 1600;
	public static int HEIGHT = 900;
	private Random rand = new Random();
	public static List<Atom> atoms = new ArrayList<>();
	private int ammount_of_atoms = 10;
	private int start_speed_value = 3;
	public static final double kb = 1;
	public static final boolean debub_mode = true;
	//private int temperature = 20; 
	//Average_Ekin = 3/2 k * T
	//Average_EKin = sum ( mi * v² /2)i
	//=> sum ( mi * v² /2)i 2/(3*k) = T
	
	public Main(String gamename)
	{
		super(gamename);
	}
 
	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		for(int i = 0 ; i < this.ammount_of_atoms ; i++) {
			this.atoms.add(new Atom());
		}
		for (Atom atom : atoms) {
			atom.setMass(10);
			atom.setSize(60);
			Vector position = new Vector(rand.nextInt(WIDTH-atom.getSize()),rand.nextInt(HEIGHT-atom.getSize()));
			Vector velocity = new Vector(Math.random() * (start_speed_value), Math.random() * (start_speed_value));
			atom.setColor(atom.randomColor());
			atom.setPosition(position);
			atom.setVelocity(velocity);
			//System.out.println(atom.getVelocity().ToString());
		}
	}
	public double getAverageTotalSpeed(List<Atom> atoms) {
		double speed = 0;
		double y = 0;
		for (Atom atom : atoms) {
			speed = speed+ Math.sqrt(Math.pow(atom.getVelocity().x,2) + Math.pow(atom.getVelocity().y,2));
		}
		return speed/atoms.size();
	}
	public Vector getAverageSpeed(List<Atom> atoms) {
		double x = 0;
		double y = 0;
		for (Atom atom : atoms) {
			x = x + atom.getVelocity().x;
			y = y + atom.getVelocity().y;
		}
		x = x/atoms.size();
		y = y/atoms.size();
		Vector vec = new Vector(x,y);
		return vec;
	}
	public double getTemperature(List<Atom> atoms) {
		double ekin = 0;
		double kb = 1; //bolzman constant
		//Average_EKin = sum ( mi * v² /2)i
		//=> sum ( mi * v² /2)i 2/(3*k) = T
		for (Atom atom : atoms) {
			ekin = ekin + ((atom.getMass() * atom.getVelocity().getSquare())/2);
		}
		ekin = ekin/atoms.size(); //average kinecti Energy;
		return ekin*2/(3*Main.kb);
		
	}
	public double getPressure(List<Atom> atoms) {
		double volume = Main.WIDTH * Main.HEIGHT * 1;
		//P*V = N * kb* T
		return (atoms.size()*Main.kb*this.getTemperature(Main.atoms))/volume;
	}
	
	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		for (Atom atom : atoms) {
			atom.update(gameContainer, delta);
		} 
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException
	{
		for (Atom atom : atoms) {
			atom.render(gameContainer, graphics);
		}
		graphics.setColor(Color.white);
		graphics.drawString("Average Total Speed: " + Math.round(this.getAverageTotalSpeed(Main.atoms)  * 100.0) / 100.0 , Main.WIDTH-350.0f,50.0f);
		Vector vec = this.getAverageSpeed(Main.atoms);
		graphics.drawString("Average x-Speed : " +  Math.round(vec.x* 100.0) / 100.0 , Main.WIDTH-350.0f,70.0f);
		graphics.drawString("Average y-Speed : " + Math.round(vec.y* 100.0) / 100.0 , Main.WIDTH-350.0f,90.0f);
		graphics.drawString("Temperature : " + Math.round(this.getTemperature(Main.atoms)* 100.0) / 100.0 , Main.WIDTH-350.0f,110.0f);
		graphics.drawString("Pressure : " + this.getPressure(Main.atoms) , Main.WIDTH-350.0f,130.0f);
		graphics.drawString("Volume : " + Main.HEIGHT*Main.HEIGHT , Main.WIDTH-350.0f,150.0f);
		graphics.drawString("average free path: not implemented",Main.WIDTH-350.0f,170.0f);
	}

	
	
	public static void main(String[] args)
	{
		try
		{
			AppGameContainer appgc;
			appgc = new AppGameContainer(new Main("Simple Slick Game"));
			appgc.setDisplayMode(Main.WIDTH, Main.HEIGHT, false);
			appgc.setTargetFrameRate(60);
			appgc.start();
		}
		catch (SlickException ex)
		{
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public static List<Atom> getAtoms(Atom caller) {
		ArrayList<Atom> temp = new ArrayList<>(Main.atoms);
		temp.remove(caller);//removes this Atom because this would allways intersect with itself;
		return temp;
	}
}