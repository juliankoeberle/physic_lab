import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class Atom implements Node{
	
	
	
	private Vector position = new Vector(0,0);
	private Vector velocity = new Vector(0,0);
	private int mass = 0;
	private int size = 0;
	private Color color;
	private Shape shape = new Circle((float)this.position.x, (float)this.position.y, this.size/2);
	
	public Atom() {
		//no init values
	}
	
	
	public Atom(int mass, Vector position, Vector velocity) {
		this.mass = mass;
		this.size = this.mass*this.mass; //the square of the mass
		this.position = position;
		this.velocity = velocity;
	}
	
	
	@Override
	public void update(GameContainer gameContainer, int delta) {
		//100% elastic intersections
		//impuls convervation and energy conservation;
		// I : Ekin1 + Ekin2 = Ekin1' + Ekin2'
		// II : p1 + p2 = p1' + p2' 
	
		
		// v1' and v2' are vectors
		
		//v1 is this velocity
		//m1 is this mass
		
		for (Atom atom : Main.getAtoms(this)) {
			
			this.getIntersectionPoint(atom);
			if (this.getIntersectionPoint(atom) != null) {
				this.velocity = Vector.addVector(this.velocity,atom.velocity);
				atom.velocity = new Vector(0,0);
				break;
			}
			
			
			/*if (this.shape.intersects(atom.getShape())) {
				this.shape.setCenterX((float)atom.position.x+atom.getSize()+this.size);
				this.shape.setCenterY((float)atom.position.y+atom.getSize()+this.size);
				this.position.x = this.shape.getX();
				this.position.y = this.shape.getY();
				double vx = this.velocity.x;
				double vy = this.velocity.y;
				
				
				double avx = atom.velocity.x;
				double avy = atom.velocity.y;
				
				//java bug long in a row calculations output the wrong value;
				double a = 2*(this.mass*vx+atom.mass*avx);
				double b = (this.mass+atom.mass);
				double c = (a/b)-avx;
				
				c = this.velocity.x - atom.velocity.x;
				
				//System.out.println(c);
				//System.out.println(vx);
				//System.exit(1);
				
				//vx = 2*(this.mass*vx+atom.mass*avx)/(this.mass+atom.mass)-avx;
				//vy = 2*(this.mass*vy+atom.mass*avy)/(this.mass+atom.mass)-avy;
				
				double d = 2*(this.mass*vy+atom.mass*avy);
				double e = (this.mass+atom.mass);
				double f = (d/e)-avy;
				
				//f = this.velocity.y - atom.velocity.y;
				
				//avx = 2*(this.mass*vx+atom.mass*avx)/(this.mass+atom.mass)-vx;
				//avy = 2*(this.mass*vy+atom.mass*avy)/(this.mass+atom.mass)-vy;
				
				
				double g = 2*(this.mass*vx+atom.mass*avx);
				double h = (this.mass+atom.mass);
				double j = (g/h)-vx;
				
				
				double k = 2*(this.mass*vy+atom.mass*avy);
				double l = (this.mass+atom.mass);
				double m = (k/l)-vy;
				
				//System.out.println(this.velocity.x + " " + vx);
				//System.out.println(this.velocity.ToString()+ ":"+ new Vector(vx,vy).ToString());
				//System.exit(1);
			
				this.velocity = new Vector(0,0);
				atom.velocity = new Vector(0,0);
				
				
			}*/
		}
	
		if (position.x <= 0 ||position.x >=  Main.WIDTH-this.size) { //left wall
			this.velocity.x = -this.velocity.x;
		}
		if (position.y <= 0 ||position.y >=  Main.HEIGHT-this.size) { //right wall
			this.velocity.y = -this.velocity.y;
		}
		
		this.position = Vector.addVector(this.position, this.velocity);
		
	}
	
	public Vector getIntersectionPoint(Atom atom) {
		Vector r = Vector.subVector(atom.position, this.position);
		double value = r.getValue();
		double intersection = (this.size/2+atom.size/2)-value;
		if (value < this.size/2+atom.size/2) {
			if (Main.debub_mode) {
			//moves this.atom out of the first atom
			System.out.println("------");
			//System.out.println(this.position.ToString());
			//System.out.println(Vector.multiplyVectorByScalar(r.getUnitVector(), intersection+0.1).ToString());
			System.out.println(Vector.addVector(Vector.multiplyVectorByScalar(r.getUnitVector(), intersection+1),this.position).ToString());
			//this.setCenterVector(Vector.addVector(r, Vector.multiplyVectorByScalar(r.getUnitVector(), diff+0.01)));
			//this.setCenterVector(this.getCenterVector());
			//this.position = Vector.addVector(Vector.multiplyVectorByScalar(r.getUnitVector(), intersection),this.position);
			}
			return Vector.addVector(Vector.multiplyVectorByScalar(r.getUnitVector(),this.size/2),this.position);
		}
		return null;
		
	}


	@Override
	public void render(GameContainer gameContainer, Graphics graphics) {
		graphics.setColor(this.color);
		if (Main.debub_mode) {
			graphics.drawString(this.position.ToString(),(float)this.position.x-60, (float)this.position.y-80);
		}
		this.shape = new Circle((float)this.position.x, (float)this.position.y, this.size/2);
		graphics.fill(shape);
	}
	
	public Color randomColor() {
		Random rand = new Random();
		int randomNumber = rand.nextInt(12);
		if (randomNumber == 1)
			return Color.pink;
		if (randomNumber == 2)
			return Color.blue;
		if (randomNumber == 3)
			return Color.cyan;
		if (randomNumber == 4)
			return Color.darkGray;
		if (randomNumber == 5)
			return Color.gray;
		if (randomNumber == 6)
			return Color.green;
		if (randomNumber == 7)
			return Color.lightGray;
		if (randomNumber == 8)
			return Color.magenta;
		if (randomNumber == 9)
			return Color.orange;
		if (randomNumber == 10)
			return Color.pink;
		if (randomNumber == 11)
			return Color.red;
		if (randomNumber == 12)
			return Color.white;
		if (randomNumber == 13)
			return Color.yellow;
		return color;
	}


	public Vector getPosition() {
		return position;
	}


	public void setPosition(Vector position) {
		this.position = position;
	}


	public Vector  getVelocity() {
		return velocity;
	}


	public void setVelocity(Vector velocity) {
		this.velocity = velocity;
	}


	public int getMass() {
		return mass;
	}


	public void setMass(int mass) {
		this.mass = mass;
	}


	public int getSize() {
		return size;
	}


	public void setSize(int size) {
		this.size = size;
	}


	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
	}


	public Shape getShape() {
		return shape;
	}


	public void setShape(Shape shape) {
		this.shape = shape;
	}
	/*
	public Vector getCenterVector() {
		return new Vector(this.shape.getCenterX(), this.shape.getCenterY());
	}
	public void setCenterVector(Vector vec) {
		this.shape.setCenterX((float)vec.x);
		this.shape.setCenterY((float)vec.y);
		this.position.x = this.shape.getX();
		this.position.y = this.shape.getY();
	}*/
	
	


}
