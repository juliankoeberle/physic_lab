
public class Vector {
	public double x; 
	public double y; 
	
	public Vector(double x, double y) { 
		this.x = x; 
	    this.y = y; 
	} 
	
	public static Vector addVector(Vector vec1, Vector vec2) {
		return new Vector(vec1.x+vec2.x, vec1.y+vec2.y);
	}
	public static Vector subVector(Vector vec1, Vector vec2) {
		return new Vector(vec1.x-vec2.x, vec1.y-vec2.y);
	}
	public static Vector multiplyVectorByScalar(Vector vec1, double scalar) {
		return new Vector(vec1.x*scalar, vec1.y*scalar);
	}
	public static Vector devideVectorByScalar(Vector vec1, double scalar) {
		return new Vector(vec1.x/scalar, vec1.y/scalar);
	}
	public double getValue() {
		return Math.sqrt(Math.pow(this.x,2) + Math.pow(this.y, 2));
	}
	
	public Vector getUnitVector() {
		return Vector.devideVectorByScalar(this, this.getValue());
	}
	
	public double getSquare() {
		//return x scalar porduct vec*vec
		return (this.x*this.x)+(this.y*this.y);
	}
	
	public String ToString() {
		return this.x + ", "+ this.y;
	}
}
